void settings () {
  System.setProperty("jogl.disable.openglcore", "false");
  size(1440, 890, P3D);
}
int r;
int x,y,z;
int anterior = -1;
void setup(){
  frameRate(10);
  r = 20;
  x = 0;
  y = 0;
  z = 0;
  translate(width/2, height/2, 0);
  box(r);
  pushMatrix();
}
void draw() {
  //fill(random(255));
  //translate(random(width), random(height), -random(1000));
  popMatrix();
  
  float rand = random(6);
  while (floor(rand) == anterior)  rand = random(6);
  if (rand < 1) {
    x=r; y=0; z=0;
    anterior = 0;
  }
  else if (rand < 2) {
    x=-r; y=0; z=0;
    anterior = 1;
  }
  else if (rand < 3) {
    x=0; y=r; z=0;
    anterior = 2;
  }
  else if (rand < 4) {
    x=0; y=-r; z=0;
    anterior = 3;
  }
  else if (rand < 5) {
    x=0; y=0; z=r;
    anterior = 4;
  }
  else {
    x=r; y=0; z=-r;
    anterior = 5;
  } 
    
  translate (x,y,z);
  box(r);
  pushMatrix();
}
